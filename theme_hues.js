/**
 *
 * Javascript sugar for Theme Hues forms
 */

$(document).ready(function(){
  $('input').css({'background-color': '#000000!important'});

  $('input[name^=region]').bind('blur', function(){
    var rid = Number($(this).attr('name').replace('region-', ''));
    var region = theme_hues_get_region(rid);
    if (!region)
      return;

    for (var i = 0; i < region.selectors.length; i++) {
      $(region.selectors[i].selector).attr("style", region.selectors[i].property + ': #' + $(this).val() + ' ! important');
    }
  });
});

function theme_hues_get_region(rid) {
  for (var i = 0; i < theme_hues_selectors.length; i++) {
    if(theme_hues_selectors[i].rid == rid)
      return theme_hues_selectors[i];
  }

  return null;
}