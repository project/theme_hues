
-- SUMMARY --

Theme Hues uses collections of css selectors to allow users to change their theme colors and background images.
Collections of selectors, or regions, can be associated with a single color.  This allows the theme developer to group theme elements chromatically (all menu items, all headers, or all text that needs special attention, for instance) and allow users to alter colors for those specific groups.  Not only does this promote good ui practice, it avoids the MySpace affliction that can come when non-designers are given too much power.

Simply put, the theme designer or site administrator can specify collections of CSS selectors, give them a name, and allow the user to change the color for that group of selectors.

Further, designers can designate other selectors so that users can upload an image and use that image as the
background-image property of that element.

Theme Hues saves the users choices in a stylesheet called 'theme_hues.css' and injects it into the page header.

The administrator may decide whether user settings are visible to all, to only the user, or while viewing content the user has created.

This module requires the jscolor JavaScript color picker library.  You can download it here: http://jscolor.com
Unzip in this directory so that the javascript file ends up in 'yoursite/sites/all/modules/theme_hues/jscolor'.

For a full description of the module, visit the project page:
  http://drupal.org/project/theme_hues

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/theme_hues


-- REQUIREMENTS --

jcolor JavaScript color picker.  Available at http://jscolor.com.  See below for installation instructions.
jQuery Update module.


-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.

Once installed, add jcolor to the module directory.

Download the JavaScript color picker script (http://jscolor.com) and unpack into this directory (ie. yoursite/sites/all/modules/theme_hues/jscolor).


-- CONFIGURATION --

* Configure what users may change in your theme css, navigate to Theme Hues Administration.  Follow the instructions to define collections of css selectors and label them.  Users can then associate a color to each collection of selectors (regions).  Administrators may also associate background-images to selectors.

 * Users may associate colors to regions by navigating to the Theme Hues link in the navigation menu.  Users may also use this form to upload images according to the settings made in the administration form.

-- TROUBLESHOOTING --

If the styles seem to not be updating, make sure you reload the page from the server.  Most browsers store stylesheets in their cache, so Shift-click reload or empty your browser cache to make sure you're seeing the latest version of the stylesheet.

-- CONTACT --

Current maintainer:
* Aaron Craig (aacraig) - http://drupal.org/user/118575

This project was developed for Evolving Design's Artist Sites project:
http://evolving-design.biz/the-passionate-artist
